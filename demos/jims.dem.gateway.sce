// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011 - DIGITE - Allan CORNET
//
// This file is released under the 3-clause BSD license. See COPYING-BSD.

function subdemolist = demos_gui_jims(subdemolist)

  demopath = get_absolute_file_path("jims.dem.gateway.sce");

  subdemolist = ["demo basic"             ,"basic.dem.sce"; ..
                 "demo swing"             ,"swing.dem.sce"];

  subdemolist(:,2) = demopath + subdemolist(:,2);
endfunction   

subdemolist = demos_gui_jims();
clear demos_gui_jims;