/*
 * JIMS ( http://forge.scilab.org/index.php/p/JIMS/ ) - This file is a part of JIMS
 * Copyright (C) 2010 - 2011 - Calixte DENIZET <calixte@contrib.scilab.org>
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */
/*--------------------------------------------------------------------------*/
#include <stdio.h>
#include "JIMS.h"
#include "OptionsHelper.h"
#include "gw_helper.h"
#include "ScilabObjects.h"
#define __USE_DEPRECATED_STACK_FUNCTIONS__
#include "stack-c.h"
#include "api_scilab.h"
#include "Scierror.h"
#include "OptionsHelper.h"
#include "MALLOC.h"
#include "getSciArgs.h"
#include "noMoreMemory.h"
/*--------------------------------------------------------------------------*/
#define FUN_DECL "y=%s(varargin)"
#define FUN_BODY "y=jinvoke_lu(int32(%i),\"%s\",varargin)"
/*--------------------------------------------------------------------------*/
int sci_jdeff(char *fname)
{
    SciErr err;
    char *className = NULL;
    char *methName = NULL;
    char *errmsg = NULL;
    int ret = 0;
    char *funName = NULL;
    char *def = NULL;
    char *code = NULL;

    CheckRhs(3, 3);

    setCopyOccured(0);
    initialization();
    setIsNew(0);

    className = getSingleString(1, fname);
    if (!className)
    {
        return 0;
    }

    methName = getSingleString(2, fname);
    if (!methName)
    {
        freeAllocatedSingleString(className);
        return 0;
    }


    // the second parameter of loadjavaclass indicates that we don't expect a class reloading
    ret = loadjavaclass(className, 0, &errmsg);
    freeAllocatedSingleString(className);

    if (errmsg)
    {
        Scierror(999, JAVAERROR, fname, errmsg);
        freeAllocatedSingleString(methName);
        FREE(errmsg);
        return 0;
    }

    funName = getSingleString(3, fname);
    if (!funName)
    {
        freeAllocatedSingleString(methName);
        return 0;
    }

    def = (char*)MALLOC(sizeof(char) * (strlen(funName) + strlen(FUN_DECL) + 1));
    if (!def)
    {
        freeAllocatedSingleString(methName);
        freeAllocatedSingleString(funName);
        Scierror(999, "%s: %s\n", fname, NOMOREMEMORY);
        return 0;
    }

    err = createMatrixOfString(pvApiCtx, ONE, 1, 1, (const char * const *)&funName);
    if (err.iErr)
    {
        freeAllocatedSingleString(funName);
        freeAllocatedSingleString(methName);
        FREE(def);
        printError(&err, 0);
        return 0;
    }

    sprintf(def, FUN_DECL, funName);
    err = createMatrixOfString(pvApiCtx, TWO, 1, 1, (const char * const *)&def);
    if (err.iErr)
    {
        FREE(def);
        printError(&err, 0);
        return 0;
    }

    freeAllocatedSingleString(funName);

    code = (char*)MALLOC(sizeof(char) * (strlen(methName) + strlen(FUN_BODY) + 1));
    if (!code)
    {
        Scierror(999, "%s: %s\n", fname, NOMOREMEMORY);
        return 0;
    }

    sprintf(code, FUN_BODY, ret, methName);
    err = createMatrixOfString(pvApiCtx, THREE, 1, 1, (const char * const *)&code);
    FREE(def);
    FREE(code);

    if (err.iErr)
    {
        freeAllocatedSingleString(methName);
        printError(&err, 0);
        return 0;
    }

    SciString(&ONE, "!_deff_wrapper", &ONE, &THREE);
    freeAllocatedSingleString(methName);

    LhsVar(1) = 0;
    PutLhsVar();
    return 0;
}
/*--------------------------------------------------------------------------*/
