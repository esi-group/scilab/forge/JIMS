/*
 * JIMS ( http://forge.scilab.org/index.php/p/JIMS/ ) - This file is a part of JIMS
 * Copyright (C) 2010 - 2011 - Calixte DENIZET <calixte@contrib.scilab.org>
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */
/*--------------------------------------------------------------------------*/
#include "OptionsHelper.h"
#include "ScilabObjects.h"
#include "gw_helper.h"
#include "JIMS.h"
#define __USE_DEPRECATED_STACK_FUNCTIONS__
#include "stack-c.h"
#include "api_scilab.h"
#include "Scierror.h"
#include "MALLOC.h"
/*--------------------------------------------------------------------------*/
int sci_jwrapinchar(char *fname)
{
    SciErr err;
    int *addr = NULL, *tmpvar = NULL;
    int i = 1, type = 0;

    setCopyOccured(0);
    initialization();
    setIsNew(0);

    if (Rhs == 0)
    {
        Scierror(999,"%s: Wrong number of input arguments: 1 or more expected\n", fname);
        return 0;
    }

    CheckLhs(Rhs, Rhs);

    tmpvar = (int*)MALLOC(sizeof(int) * (Rhs + 1));
    *tmpvar = 0;

    for (;i <= Rhs; i++)
    {
        int id = 0, row = 0, col = 0;
        unsigned short *data = NULL;

        err = getVarAddressFromPosition(pvApiCtx, i, &addr);
        if (err.iErr)
        {
            removeTemporaryVars(tmpvar);
            FREE(tmpvar);
            printError(&err, 0);
            return 0;
        }

        err = getVarType(pvApiCtx, addr, &type);
        if (err.iErr)
        {
            removeTemporaryVars(tmpvar);
            FREE(tmpvar);
            printError(&err, 0);
            return 0;
        }

        if (type != sci_ints)
        {
            removeTemporaryVars(tmpvar);
            FREE(tmpvar);
            Scierror(999, "%s: Wrong argument type at position %i: uint16 expected\n", fname, i);
            return 0;
        }

        err = getMatrixOfIntegerPrecision(pvApiCtx, addr, &type);
        if (err.iErr)
        {
            removeTemporaryVars(tmpvar);
            FREE(tmpvar);
            printError(&err, 0);
            return 0;
        }

        if (type != SCI_UINT16)
        {
            removeTemporaryVars(tmpvar);
            FREE(tmpvar);
            Scierror(999, "%s: Wrong argument type at position %i: uint16 expected\n", fname, i);
            return 0;
        }

        err = getMatrixOfUnsignedInteger16(pvApiCtx, addr, &row, &col, &data);
        if (err.iErr)
        {
            removeTemporaryVars(tmpvar);
            FREE(tmpvar);
            printError(&err, 0);
            return 0;
        }

        if (row == 0 || col == 0)
        {
            id = 0;
        }
        else if (row == 1 && col == 1)
        {
            id = wrapSingleChar(data[0]);
        }
        else if (row == 1)
        {
            id = wrapRowChar(data, col);
        }
        else
        {
            id = wrapMatChar(data, row, col);
        }

        tmpvar[++tmpvar[0]] = id;

        if (!createJavaObjectAtPos(_JOBJ, Rhs + i, id))
        {
            removeTemporaryVars(tmpvar);
            FREE(tmpvar);
            return 0;
        }

        LhsVar(i) = Rhs + i;
    }

    PutLhsVar();

    FREE(tmpvar);

    return 0;
}
/*--------------------------------------------------------------------------*/
