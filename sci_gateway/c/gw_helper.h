/*
 * JIMS ( http://forge.scilab.org/index.php/p/JIMS/ ) - This file is a part of JIMS
 * Copyright (C) 2011 - Allan CORNET
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */
/*--------------------------------------------------------------------------*/
#ifndef __GW_HELPER_H__
#define __GW_HELPER_H__
/*--------------------------------------------------------------------------*/
void setMethodName(const char* _methodName);
char * getMethodName(void);
void freeMethodName(void);

void setObjectId(int _objectId);
int getObjectId(void);

void setIsNew(const char  _isNew);
char getIsNew(void);
/*--------------------------------------------------------------------------*/
#endif /* __GW_HELPER_H__ */
