/*
 * JIMS ( http://forge.scilab.org/index.php/p/JIMS/ ) - This file is a part of JIMS
 * Copyright (C) 2010 - 2011 - Calixte DENIZET <calixte@contrib.scilab.org>
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */
/*--------------------------------------------------------------------------*/
#include "JIMS.h"
#include "OptionsHelper.h"
#include "gw_helper.h"
#include "ScilabObjects.h"
#define __USE_DEPRECATED_STACK_FUNCTIONS__
#include "stack-c.h"
#include "api_scilab.h"
#include "Scierror.h"
#include "MALLOC.h"
#include "getSciArgs.h"
/*--------------------------------------------------------------------------*/
/**
 * Get a named field in an object
 * - 1: object or scilab var to be wrapped
 * - 2: field name
 * - [3]: a boolean to indicate if the result must be unwrapped (false by default)
 */
int sci_jgetfield(char *fname)
{
    SciErr err;
    int tmpvar[2] = {0, 0};
    int *addr = NULL;
    int mustUnwrap = 0;
    int idObj = 0;
    int ret = 0;
    char *fieldName = NULL;
    char *errmsg = NULL;

    CheckRhs(2, 3);
    CheckLhs(1, 1);

    setCopyOccured(0);
    initialization();
    setIsNew(0);

    err = getVarAddressFromPosition(pvApiCtx, 1, &addr);
    if (err.iErr)
    {
        printError(&err, 0);
        return 0;
    }

    idObj = getIdOfArg(addr, fname, tmpvar, 0, 1);
    if (idObj == -1)
    {
        return 0;
    }

    fieldName = getSingleString(2, fname);
    if (!fieldName)
    {
        return 0;
    }

    if (Rhs == 3)
    {
        err = getVarAddressFromPosition(pvApiCtx, 3, &addr);
        if (err.iErr)
        {
            freeAllocatedSingleString(fieldName);
            printError(&err, 0);
            return 0;
        }

        if (!getScalarBoolean(pvApiCtx, addr, &mustUnwrap))
        {
            freeAllocatedSingleString(fieldName);
            return 0;
        }
    }

    ret = getfield(idObj, fieldName, &errmsg);
    freeAllocatedSingleString(fieldName);
    removeTemporaryVars(tmpvar);

    if (errmsg)
    {
        Scierror(999, JAVAERROR, fname, errmsg);
        FREE(errmsg);
        return 0;
    }

    if (mustUnwrap && unwrap(ret, Rhs + 1))
    {
        removescilabjavaobject(ret);
    }
    else if (!createJavaObjectAtPos(_JOBJ, Rhs + 1, ret))
    {
        removescilabjavaobject(ret);
        return 0;
    }

    LhsVar(1) = Rhs + 1;
    PutLhsVar();

    return 0;
}
/*--------------------------------------------------------------------------*/
