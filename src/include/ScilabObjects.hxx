/*
 * JIMS ( http://forge.scilab.org/index.php/p/JIMS/ ) - This file is a part of JIMS
 * Copyright (C) 2010 - 2011 - Calixte DENIZET <calixte@contrib.scilab.org>
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

#ifndef __SCILABOBJECTS_HXX__
#define __SCILABOBJECTS_HXX__
/*--------------------------------------------------------------------------*/
#include <jni.h>

extern "C"
{
#define _GLOBAL_VARS_
#include "getScilabJavaVM.h"
#include "ScilabObjects.h"
} /* extern "C" */

#define unwrap(kind,Kind,type,ScilabType,JNIType,Type) void unwrap##kind##type (int id, int pos, char **errmsg) \
    {                                                                   \
        JavaVM *vm_ = getScilabJavaVM ();                               \
        if (vm_)                                                        \
        {                                                               \
            try                                                         \
            {                                                           \
                unwrap##Kind<JNIType,ScilabType,__JIMS__Scilab##Type##__>(vm_, getMethodOfConv(), id, pos); \
            }                                                           \
            catch (char const *e)                                       \
            {                                                           \
                *errmsg = strdup(e);                                    \
            }                                                           \
        }                                                               \
    }

#define unwrapForType(type,ScilabType,JNIType,Type) unwrap(,Single,type,ScilabType,JNIType,Type) \
    unwrap(row,Row,type,ScilabType,JNIType,Type)                        \
    unwrap(mat,Mat,type,ScilabType,JNIType,Type)


#define wrapSingleCall(type, Type) int wrapSingle##Type(type x)         \
    {                                                                   \
        return wrapSingle<type, __JIMS__ScilabAnOtherWrapper##Type##__>(x); \
    }

#define wrapRowCall(type, Type) int wrapRow##Type(type *x, int len)     \
    {                                                                   \
        return wrapRow<type, __JIMS__ScilabAnOtherWrapper##Type##__>(x, len); \
    }

#define wrapMatCall(type, Type) int wrapMat##Type(type *x, int r, int c) \
    {                                                                   \
        return wrapMat<type, __JIMS__ScilabAnOtherWrapper##Type##__>(x, r, c); \
    }

#define wrapForType(type, Type) wrapSingleCall(type,Type)       \
    wrapRowCall(type,Type)                                      \
    wrapMatCall(type,Type)


#define wrapSingleWithCastCall(type, castType, Type) int wrapSingle##Type(type x) \
    {                                                                   \
        return wrapSingleWithCast<type, castType, __JIMS__ScilabAnOtherWrapper##Type##__>(x); \
    }

#define wrapRowWithCastCall(type, castType, Type) int wrapRow##Type(type *x, int len) \
    {                                                                   \
        return wrapRowWithCast<type, castType, __JIMS__ScilabAnOtherWrapper##Type##__>(x, len); \
    }

#define wrapMatWithCastCall(type, castType, Type) int wrapMat##Type(type *x, int r, int c) \
    {                                                                   \
        return wrapMatWithCast<type, castType, __JIMS__ScilabAnOtherWrapper##Type##__>(x, r, c); \
    }

#define wrapWithCastForType(type, castType, Type) wrapSingleWithCastCall(type,castType,Type) \
    wrapRowWithCastCall(type,castType,Type)                             \
    wrapMatWithCastCall(type,castType,Type)

#define wrapIntoDirectBuffer(Type,type) void* wrapAsDirect##Type##Buffer(type *addr, long size, int *id) \
    {                                                                   \
        JavaVM *vm_ = getScilabJavaVM ();                               \
        if (vm_)                                                        \
        {                                                               \
            return ScilabJavaObjectBis::wrapAsDirectBuffer<type>(vm_, addr, size, id); \
        } \
        return NULL;                                                               \
    }

/*--------------------------------------------------------------------------*/
#endif /* __SCILABOBJECTS_HXX__ */
