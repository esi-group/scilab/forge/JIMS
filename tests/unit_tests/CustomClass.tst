//
//  Copyright (C) 2012 - Scilab Enterprises
//
//  This file must be used under the terms of the CeCILL.
//  This source file is licensed as described in the file COPYING, which
//  you should have received as part of this distribution.  The terms
//  are also available at
//  http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
//
// <-- JVM MANDATORY -->
//

CustomClassPath = jimsgetpath() + '/tests/unit_tests';
javaclasspath(CustomClassPath);

jimport CustomClass;

moc = ['cr' 'rc']
jautoUnwrap(%T);
N=30;

function DISP_PERF(str)
    // uncomment for performance data
    // disp(str)
endfunction

// Direct buffers
timer();
for i=1:N,for j=1:N,a=rand(i,j);b=a+1;jinvoke_db(CustomClass, "F", "a");if a <> b then,disp("bug");end;end;end
for i=1:N,for j=1:N,a=int8(10*rand(i,j));b=a+1;jinvoke_db(CustomClass, "F", "a");if a <> b then,disp("bug");end;end;end
for i=1:N,for j=1:N,a=int16(100*rand(i,j));b=a+1;jinvoke_db(CustomClass, "F", "a");if a <> b then,disp("bug");end;end;end
for i=1:N,for j=1:N,a=int32(1000*rand(i,j));b=a+1;jinvoke_db(CustomClass, "F", "a");if a <> b then,disp("bug");end;end;end
for i=1:N,for j=1:N,a=uint8(10*rand(i,j));b=a+1;jinvoke_db(CustomClass, "F", "a");if a <> b then,disp("bug");end;end;end
for i=1:N,for j=1:N,a=uint16(100*rand(i,j));b=a+1;jinvoke_db(CustomClass, "F", "a");if a <> b then,disp("bug");end;end;end	
for i=1:N,for j=1:N,a=uint32(1000*rand(i,j));b=a+1;jinvoke_db(CustomClass, "F", "a");if a <> b then,disp("bug");end;end;end		
DISP_PERF("By direct buffers=" + string(timer()))

for m=moc
    jconvMatrixMethod(m);
    timer();
    for i=1:N,for j=1:N,a=rand(i,j);if CustomClass.F(a) <> a then,disp("bug");end;end;end
    for i=1:N,for j=1:N,a=int8(10*rand(i,j));if CustomClass.F(a) <> a then,disp("bug");end;end;end
    for i=1:N,for j=1:N,a=int16(100*rand(i,j));if CustomClass.F(a) <> a then,disp("bug");end;end;end
    for i=1:N,for j=1:N,a=int32(1000*rand(i,j));if CustomClass.F(a) <> a then,disp("bug");end;end;end
    for i=1:N,for j=1:N,a=uint8(10*rand(i,j));if CustomClass.F(a) <> a then,disp("bug");end;end;end
    for i=1:N,for j=1:N,a=uint16(100*rand(i,j));if CustomClass.F(a) <> a then,disp("bug");end;end;end
    for i=1:N,for j=1:N,a=uint32(1000*rand(i,j));if CustomClass.F(a) <> a then,disp("bug");end;end;end
    DISP_PERF("By copy=" + string(timer()))
    
    for i=1:N,for j=1:N,a=string(rand(i,j));if CustomClass.F(a) <> a then,disp("bug");end;end;end   
    for i=1:N,for j=1:N,b=rand(i,j);a=jwrapinfloat(b);if abs(CustomClass.F(a)-b) > 1E-6 then,disp("bug");end;jremove(a);end;end
    for i=1:N,for j=1:N,b=uint16(1024*rand(i,j));a=jwrapinchar(b);if CustomClass.F(a) <> b then,disp("bug");end;jremove(a);end;end
    for i=1:N,for j=1:N,a=(floor(2*rand(i,j))==1);if CustomClass.F(a) <> a then,disp("bug");end;end;end
end
    
t = CustomClass.new("Hello World");
t.getField()
t.field = "Dlrow Olleh";
t.field
t.getField()
t.setField("Why ??")
t.getField()
t.returnUTF8String("Γεια σας κόσμο")


